import { writable } from "svelte/store"
import { logE, logW, logF, logT, logI } from "../services/log"
let storeContent = []

function createVideoPoolStore() {
    const { subscribe, set, update } = writable(storeContent, start)

    // Zur Zeit funktionslos, nur fürs Store-Verständnis...
    function start() {
        console.log("START VideoPoolStore")
        return stop
    }
    function stop() {
        console.log("STOP VideoPoolStore")
        return
    }

    function initPool(count) {
        if (storeContent.length < count) {
            logT(`VIDEOPOOLSTORE + ${count-storeContent.length} VIDEOS`)
            for (let index = storeContent.length; index < count; index++) {
                storeContent.push({ id: `video${index + 1}`, descriptor: "", url: "", state: "", hlsVideo: null })
            }
            update((store) => {
                store = storeContent
                return store
            })
        }
    }

    function setState(id, state) {
        update((store) => {
            let item = store.find((o) => o.id === id)
            if (item) item.state = state
            return store
        })
    }

    return {
        subscribe,
        set,
        initPool: (count) => initPool(count),
        setState: (id, state) => setState(id, state),
    }
}

export const videoPoolStore = createVideoPoolStore()

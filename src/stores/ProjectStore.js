import { readable } from "svelte/store"

const urlDirectorApi = "https://api.inlume.io"
//const urlDirectorApi = 'http://localhost:8000'
const projectCache = []

function createRepositoryStore() {
    const { subscribe } = readable([], start)

    // Zur Zeit funktionslos, nur fürs Store-Verständnis...
    function start() {
        console.log("START ProjectStore")
        return stop
    }
    function stop() {
        console.log("STOP ProjectStore")
        return
    }

    async function getProjects(customerId) {
        let projects
        let fetched = await fetch(`${urlDirectorApi}/firestore/projects`, {
            headers: {
                db: "dev",
                id: customerId, //'3twvbvRfo5KthP9wxQVX',
            },
        })
        projects = await fetched.json()
        return projects
    }

    async function getProjectData(projectId) {
        let projectData = projectCache.find((o) => o.id === projectId)
        if (!projectData) {
            let fetched = await fetch(`${urlDirectorApi}/firestore/project`, {
                headers: {
                    db: "dev",
                    id: projectId,
                },
            })
            projectData = await fetched.json()
            projectData["id"] = projectId
            projectCache.push(projectData)
        }
        return projectData
    }

    return {
        subscribe,
        getProjects: (customerId) => getProjects(customerId),
        getProjectData: (projectId) => getProjectData(projectId),
    }
}

export const ProjectStore = createRepositoryStore()

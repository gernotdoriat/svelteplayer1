import { logE, logW, logF, logT, logI } from "../services/log"
import { writable } from "svelte/store"
import { resolveLanguageObject } from "../services/languageService"

let storeContent = {}

function createStore() {
    const { subscribe, update } = writable(storeContent, start)

    function start() {
        logF("START LanguageStore")
        return stop
    }
    function stop() {
        logF("STOP LanguageStore")
        return
    }

    return {
        subscribe,

        // setzt das Array der verfügbaren Sprachen
        setLanguages: (languages) => {
            storeContent["languages"] = languages
            update((store) => {
                store = storeContent
            })
        },

        // setzt die aktuelle Sprache
        setLanguage: (language) => {
            storeContent["language"] = language
            update((store) => {
                store = storeContent
            })
        },

        // holt ein beliebiges Feld - Objekt wie nicht-Objekt
        getField: (field) => {
            return resolveLanguageObject(field, storeContent.language, storeContent.languages)
        },

        // holt das 'url' Sub-Feld
        getClipUrl: (field) => {
            let clipField = resolveLanguageObject(field, storeContent.language, storeContent.languages)
            return clipField["url"]
        },
    }
}

export const LanguageStore = createStore()

import { LanguageStore } from "../stores/LanguageStore"

export function getVideoPoolSize(playlist, format, repository) {
    let max = 0

    let scenesCount = Object.keys(playlist.scenes).length
    for (let i = 0; i < scenesCount; i++) {
        max = Math.max(max, getSceneClips(playlist, i, format, repository).length)
    }
    return max + 1
}

let uniqueIdCounter = 1

function getClips(scene, format, descriptor) {
    let sceneClips = []
    if (scene) {
        for (const field in scene) {
            if (field.startsWith("clip") && field.includes(format) && scene[field]) {
                sceneClips.push({ id: `${field} ${scene["id"]} ; ${uniqueIdCounter++}`, url: LanguageStore.getClipUrl(scene[field]), descriptor: descriptor })
            }
        }
    }
    return sceneClips
}

export function getSceneClips(playlist, playlistIndex, format, repository) {
    let scene = repository.scenes.find((o) => o.id === playlist.scenes["PlSc-" + playlistIndex].trunk.sceneId)
    let sceneClips = getClips(scene, format, `SCENE ${playlistIndex + 1}`)
    // die branches nicht aus der scene, sondern aus dem playlist Item!
    let branchClips1 = getBranchFirstSceneClips(playlist.scenes["PlSc-" + playlistIndex].branches, format, repository)
    let neighborSceneClips = getNeighborSceneClips(playlist, playlistIndex, format, repository)
    let branchClips2 = getBranchRemainingSceneClips(playlist.scenes["PlSc-" + playlistIndex].branches, format, repository)

    return [...sceneClips, ...branchClips1, ...neighborSceneClips, ...branchClips2]
}

function getNeighborSceneClips(playlist, playlistIndex, format, repository) {
    let succClips = []
    if (playlistIndex + 1 < Object.keys(playlist.scenes).length) {
        let scene = repository.scenes.find((o) => o.id === playlist.scenes["PlSc-" + (playlistIndex + 1)].trunk.sceneId)
        succClips = getClips(scene, format, `SCENE ${playlistIndex + 2}`)
    }
    let predClips = []
    if (playlistIndex > 0) {
        let scene = repository.scenes.find((o) => o.id === playlist.scenes["PlSc-" + (playlistIndex - 1)].trunk.sceneId)
        predClips = getClips(scene, format, `SCENE ${playlistIndex}`)
    }
    return [...succClips, ...predClips]
}

function getBranchFirstSceneClips(branches, format, repository) {
    let branchClips = []
    if (branches) {
        let branchesCount = Object.keys(branches).length
        for (let i = 0; i < branchesCount; i++) {
            const branchSeq = branches["BrSq-" + i]
            const branchSeqScene = branchSeq["BrSc-0"]
            let scene = repository.scenes.find((o) => o.id === branchSeqScene.sceneId)
            if (scene) {
                let sceneClips = getClips(scene, format, `BRANCH ${i + 1}.1`)
                branchClips = [...branchClips, ...sceneClips]
            }
        }
    }
    return branchClips
}

function getBranchRemainingSceneClips(branches, format, repository) {
    let branchClips = []

    if (branches) {
        let branchesCount = Object.keys(branches).length
        for (let i = 0; i < branchesCount; i++) {
            const branchSeq = branches["BrSq-" + i]
            let branchSeqScenesConut = Object.keys(branchSeq).length
            for (let i = 1; i < branchSeqScenesConut; i++) {
                const branchSeqScene = branchSeq["BrSc-" + i]
                let scene = repository.scenes.find((o) => o.id === branchSeqScene.sceneId)
                if (scene) {
                    let sceneClips = getClips(scene, format, `BRANCH ${i + 1}.1`)
                    branchClips = [...branchClips, ...sceneClips]
                }
            }
        }
    }

    return branchClips
}

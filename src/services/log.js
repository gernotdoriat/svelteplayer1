

let sessionObject={env:false}

// back log ERROR
export function logE(text) {
    let currTime = new Date()
    console.error(`B>E>${currTime.getHours()}:${currTime.getMinutes()}:${currTime.getSeconds()}:${currTime.getMilliseconds()}>${text}`)
    if (sessionObject.env != 'prod')
        alert(text)
}

// back log WARNING
export function logW(text) {
    let currTime = new Date()
    console.warn(`B>W>${currTime.getHours()}:${currTime.getMinutes()}:${currTime.getSeconds()}:${currTime.getMilliseconds()}>${text}`)
}

// back log TEST - hervorgehoben durch console.warn
export function logT(text) {
    let currTime = new Date()
    console.warn(`B>T>${currTime.getHours()}:${currTime.getMinutes()}:${currTime.getSeconds()}:${currTime.getMilliseconds()}>${text}`)
}

// back log FUNCTION - i.d.R. ENTER & EXIT
export function logF(text) {
    let currTime = new Date()
    console.log(`B>F>${currTime.getHours()}:${currTime.getMinutes()}:${currTime.getSeconds()}:${currTime.getMilliseconds()}>${text}`)
}

// back log INFO
export function logI(text) {
    let currTime = new Date()
    console.log(`B>I>${currTime.getHours()}:${currTime.getMinutes()}:${currTime.getSeconds()}:${currTime.getMilliseconds()}>${text}`)
}




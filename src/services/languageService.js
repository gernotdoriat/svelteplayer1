

// Liefert ein 'sprachenloses' Project mit Elementen in der gewünschten Sprache anstelle der Language-Objects in der Quelle
export function resolveProject(project, language) {
    return resolveObject(project, language, project.languages)
}

// typeof {} und [] ist 'object'
function isObject(item) {
    return item && typeof item === "object" && !Array.isArray(item)
}

// RECURSIV
function resolveObject(object, language, languages) {
    if (isObject(object)) {
        let result = {}
        if (isLanguageObject(object)) result = resolveLanguageObject(object, language, languages)
        else
            for (const key in object) {
                result[key] = resolveObject(object[key], language, languages)
            }
        return result
    } else if (Array.isArray(object)) {
        let array = []
        for (const item of object) {
            array.push(resolveObject(item, language, languages))
        }
        return array
    } else return object
}

// TRUE, wenn ALLE Keys 2stellige Capitals sind
function isLanguageObject(object) {
    for (const key in object) {
        if (key.length != 2) return false
        if (key != key.toUpperCase()) return false
    }
    return true
}

// Wertet ein LanguageObject aus oder gibt ein nicht-LanguageObject unverändert zurück
export function resolveLanguageObject(object, language, languages) {
    if (typeof object == "object") {
        // 1. eingestellte Sprache
        let result = object[language]
        // 2. Sprache0
        if (typeof result == "undefined") result = object[languages[0]]
        // 3. erstbeste Sprache
        if (typeof result == "undefined") {
            for (const language in object) {
                result = object[language]
                if (typeof result != "undefined") break
            }
        }
        return result
    } else return object
}
